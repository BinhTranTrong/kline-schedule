package com.kasikornbank.kline.schedule.config;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ScheduleClientConfiguration {
    @Bean
    public Request.Options options() {
        return new Request.Options(30000, 30000);
    }
}

