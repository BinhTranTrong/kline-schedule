package com.kasikornbank.kline.schedule.interfaces;

import com.kasikornbank.kline.schedule.config.ScheduleClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "schedule", configuration = ScheduleClientConfiguration.class)
public class ScheduleClient {


}
