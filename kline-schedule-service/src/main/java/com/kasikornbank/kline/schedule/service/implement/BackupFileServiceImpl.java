package com.kasikornbank.kline.schedule.service.implement;

import com.kasikornbank.kline.schedule.service.BackupFileService;
import com.kasikornbank.kline.schedule.service.ExecutingBackUpFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class BackupFileServiceImpl implements BackupFileService {

    private static Boolean IS_FIRST_RUN = true;

    @Autowired
    private ExecutingBackUpFileService executingBackUpFileService;

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Handle back up file when this job runs
     */
    @Override
    public void process() {
        log.info("=== Starting backup file service ===");
        if(IS_FIRST_RUN) {
            // On the first run, this job will be handling all log files that exist in every micro service
            executingBackUpFileService.handleFirstRunBackUpFile();
            IS_FIRST_RUN = false;
        } else {
            executingBackUpFileService.handleNextRunsBackUpFile();
        }
    }

}

