package com.kasikornbank.kline.schedule.repository;

import com.kasikornbank.kline.schedule.entity.Logging;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@Repository
public interface LoggingRepository extends MongoRepository<Logging, String> {
}
