package com.kasikornbank.kline.schedule.util;

import com.kasikornbank.kline.common.constant.LoggingConstant;
import com.kasikornbank.kline.common.util.DatetimeUtil;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
public class CompressUtil {

    /**
     * @author ha_nhat.n
     * @created 2562/04/18
     * @description Compress all the valid file in the directory and output at the given path
     * @param directoryPath
     * @param outputPath
     */
    public void zipDirectory(String directoryPath, String outputPath) throws IOException {
        File directory = new File(directoryPath);
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("The path is not a directory.");
        }
        File[] fileList = directory.listFiles();
        if (fileList.length <= 0) {
            return;
        }
        List<File> qualifiedFileList = new ArrayList<>();
        for(File file: fileList){
            if(isValidFile(file.getName())){
                qualifiedFileList.add(file);
            }
        }
        if(qualifiedFileList.size() == 0){
            return;
        }
        FileOutputStream fos = new FileOutputStream(outputPath);
        ZipOutputStream zipOutput = new ZipOutputStream(fos);
        for (File file : qualifiedFileList) {
            FileInputStream fis = new FileInputStream(file);
            ZipEntry zipEntry = new ZipEntry(file.getName());
            zipOutput.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOutput.write(bytes, 0, length);
            }
            fis.close();
            file.delete();

        }
        zipOutput.close();
        fos.close();
    }

    /**
     * @author ha_nhat.n
     * @created 2562/04/18
     * @description check if the filename is matched with rule.
     * @param filename
     * @return  @true if the filename is valid
     */
    private static boolean isValidFile(String filename) {
        if(!filename.matches(LoggingConstant.CONTAIN_NUMBER)
                || !filename.endsWith(LoggingConstant.JSON_FILE)){
            return false;
        }
        LocalDateTime currentTime = LocalDateTime.now();
        LocalDateTime dateOfFile = DatetimeUtil.getDateTimeFromFileName(filename);
        LocalDateTime twentyFourHoursBefore = currentTime.minusHours(24);
        if (dateOfFile.isAfter(currentTime) || dateOfFile.isBefore(twentyFourHoursBefore)) {
            return false;
        }
        return true;
    }

    public void moveZipFiles(String zipDirPath, String outputPath) throws IOException{
        File directory = new File(zipDirPath);
        File[] fileList = directory.listFiles();
        if(fileList != null){
            for(File file: fileList){
               Path path = Files.move(Paths.get(zipDirPath+"/"+file.getName()), Paths.get(outputPath+"/"+file.getName()));
               if(path != null){
                   System.out.println("Sucessfully move.");
               }else{
                   System.out.println("Failed to move.");
               }
            }
        }
    }
}
