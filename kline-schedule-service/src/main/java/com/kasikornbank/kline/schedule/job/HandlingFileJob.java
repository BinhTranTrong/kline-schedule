package com.kasikornbank.kline.schedule.job;

import com.kasikornbank.kline.schedule.service.HandlingFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@Slf4j
@Component
public class HandlingFileJob {
    private static final String EVERY_MINUTE = "0 */1 * * * ? ";
    @Autowired
    private HandlingFileService handlingFileService;

    //    @Scheduled(fixedRateString = "${scheduled.kLine.handlingFile.fixedRate}",
//            initialDelayString = "${scheduled.kLine.handlingFile.initialDelay}")
    @Scheduled(cron = EVERY_MINUTE)
    public void processHandlingFile() {
        try {
            handlingFileService.process();
        } catch (Exception ex) {
            log.error("=== Exception from HANDLING FILE BACKGROUND JOB ===", ex);
        }
    }

}
