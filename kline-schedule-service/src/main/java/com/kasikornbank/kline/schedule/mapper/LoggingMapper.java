package com.kasikornbank.kline.schedule.mapper;

import com.kasikornbank.kline.common.dto.LoggingDto;
import com.kasikornbank.kline.schedule.entity.Logging;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@Mapper(componentModel = "spring")
public interface LoggingMapper {

    // convert Logging to LoggingDto
    LoggingDto loggingToLoggingDto (Logging logging);

    // convert LoggingDto to Logging
    Logging loggingDtoToLogging (LoggingDto loggingDto);

    // convert LoggingList to LoggingDtoList
    List<LoggingDto> loggingsToLoggingDtos (List<Logging> loggings);

}
