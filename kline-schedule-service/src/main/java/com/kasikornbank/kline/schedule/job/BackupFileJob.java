package com.kasikornbank.kline.schedule.job;

import com.kasikornbank.kline.schedule.service.BackupFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@Slf4j
@Component
public class BackupFileJob {

    @Autowired
    private BackupFileService backupFileService;

    @Scheduled(fixedRateString = "${scheduled.kLine.backupFile.fixedRate}")
    public void processBackupFile() {
        try {
            backupFileService.process();
        } catch (Exception ex) {
            log.error("=== Exception from BACKUP FILE BACKGROUND JOB ===", ex);
        }
    }

}
