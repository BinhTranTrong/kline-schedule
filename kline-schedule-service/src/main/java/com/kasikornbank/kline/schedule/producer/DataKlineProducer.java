package com.kasikornbank.kline.schedule.producer;

import com.kasikornbank.kline.common.model.DataKline;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;

@Slf4j
public class DataKlineProducer {

    @Value("${kafka.topic.data-kline}")
    private String dataKlineTopic;

    @Autowired
    private KafkaTemplate<String, DataKline> kafkaTemplate;

    public void sendDataKline(DataKline dataKline) {
        log.info("Producing data kline with message = {}", dataKline);
        kafkaTemplate.send(dataKlineTopic, dataKline);
    }

}
