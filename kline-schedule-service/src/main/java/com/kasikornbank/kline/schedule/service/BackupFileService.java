package com.kasikornbank.kline.schedule.service;

import java.io.IOException;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

public interface BackupFileService {

    void process() throws IOException;

}
