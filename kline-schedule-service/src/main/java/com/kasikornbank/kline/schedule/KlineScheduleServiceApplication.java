package com.kasikornbank.kline.schedule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@SpringBootApplication
@EnableScheduling
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.kasikornbank.kline.admin.interfaces"})
@ComponentScan(basePackages = {"com.kasikornbank.kline.common", "com.kasikornbank.kline.schedule"})
public class KlineScheduleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlineScheduleServiceApplication.class, args);
	}

}
