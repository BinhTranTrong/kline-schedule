package com.kasikornbank.kline.schedule.service;

public interface ExecutingBackUpFileService {

    void handleFirstRunBackUpFile();

    void handleNextRunsBackUpFile();

}
