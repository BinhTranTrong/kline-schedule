package com.kasikornbank.kline.schedule.entity;

import lombok.*;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Map;

/**
 * @author binh_trong.t
 * @created 2562/04/18
 */

@Document(collection = "logs")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Logging {

    @Field(value = "requestId")
    private String requestId;

    @Field(value = "common")
    private Map<String, Object> common;

    @Field(value = "requestBody")
    private Map<String, Object> requestBody;

    @Field(value = "responseBody")
    private Map<String, Object> responseBody;

}
