package com.kasikornbank.kline.schedule.service.implement;

import com.google.gson.JsonObject;
import com.kasikornbank.kline.common.constant.LoggingConstant;
import com.kasikornbank.kline.common.dto.LoggingDto;
import com.kasikornbank.kline.common.util.DatetimeUtil;
import com.kasikornbank.kline.schedule.service.ExecutingBackUpFileService;
import com.kasikornbank.kline.schedule.service.LoggingService;
import com.kasikornbank.kline.schedule.util.BackUpFileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class ExecutingBackUpFileServiceImpl implements ExecutingBackUpFileService {

    @Autowired
    private LoggingService loggingService;

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Handle the first run of back up file job
     */
    @Override
    public void handleFirstRunBackUpFile() {
        BackUpFileUtil.getDirectoryList()
                .forEach(directoryName -> BackUpFileUtil.getFileNames(directoryName).stream()
                        .forEach(fileName -> this.handleDataBackUpFile(directoryName + fileName)));
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Handle the next runs of back up file job
     */
    @Override
    public void handleNextRunsBackUpFile() {
        LocalDateTime dateTimeCurrent = LocalDateTime.now();
        BackUpFileUtil.getDirectoryList()
                .forEach(directoryName -> BackUpFileUtil.getFileNames(directoryName).stream()
                        .filter(fileName -> this.checkDateTimeFile(DatetimeUtil.getDateTimeFromFileName(fileName), dateTimeCurrent))
                        .forEach(fileName -> this.handleDataBackUpFile(directoryName + fileName)));
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Handle read file log, after that save it into mongodb and DIH via MQ
     * @param fileName
     */
    private void handleDataBackUpFile(String fileName) {
        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while(bufferedReader.ready()) {
                // convert string to json object
                JsonObject jsonObject = BackUpFileUtil.convertStringToJson(bufferedReader.readLine());
                // save data into mongodb
                loggingService.createLogging(this.setLoggingDto(jsonObject));
                // TO DO: Save data into DIH via MQ
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            log.error("Unable to open file " + fileName);
        } catch (IOException e) {
            log.error("Can not read file " + fileName);
        }
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Set data for logging dto
     * @param jsonObject
     * @return LoggingDto
     */
    private LoggingDto setLoggingDto (JsonObject jsonObject) {
        Map<String, Object> common = new HashMap<>();
        Map<String, Object> requestBody = new HashMap<>();
        Map<String, Object> responseBody = new HashMap<>();

        common.put(LoggingConstant.COMMON, jsonObject.get(LoggingConstant.COMMON));
        common.put(LoggingConstant.REQUEST_BODY, jsonObject.get(LoggingConstant.REQUEST_BODY));
        common.put(LoggingConstant.RESPONSE_BODY, jsonObject.get(LoggingConstant.RESPONSE_BODY));

        LoggingDto loggingDto = new LoggingDto();
        loggingDto.setRequestId(jsonObject.get(LoggingConstant.REQUEST_ID).toString());
        loggingDto.setCommon(common);
        loggingDto.setRequestBody(requestBody);
        loggingDto.setResponseBody(responseBody);
        return loggingDto;
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Check date time for the next runs
     * @param dateTimeFile
     * @param dateTimeCurrent
     * @return true if the file has been created within 5 minutes, vice versa return false
     */
    private boolean checkDateTimeFile(LocalDateTime dateTimeFile, LocalDateTime dateTimeCurrent) {
        return (dateTimeFile.isBefore(dateTimeCurrent) && dateTimeFile.isAfter(dateTimeCurrent.minusMinutes(5)))
                || dateTimeFile.isEqual(dateTimeCurrent);
    }

}
