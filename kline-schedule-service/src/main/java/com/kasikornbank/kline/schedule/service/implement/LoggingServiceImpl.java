package com.kasikornbank.kline.schedule.service.implement;

import com.kasikornbank.kline.common.dto.LoggingDto;
import com.kasikornbank.kline.schedule.entity.Logging;
import com.kasikornbank.kline.schedule.mapper.LoggingMapper;
import com.kasikornbank.kline.schedule.repository.LoggingRepository;
import com.kasikornbank.kline.schedule.service.LoggingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LoggingServiceImpl implements LoggingService {

    @Autowired
    private LoggingRepository loggingRepository;

    @Autowired
    private LoggingMapper loggingMapper;

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Create logging using mongodb
     * @param loggingDto
     * @return LoggingDto
     */
    @Override
    public LoggingDto createLogging(LoggingDto loggingDto) {
        Logging logging = loggingRepository.save(loggingMapper.loggingDtoToLogging(loggingDto));
        return loggingMapper.loggingToLoggingDto(logging);
    }

}
