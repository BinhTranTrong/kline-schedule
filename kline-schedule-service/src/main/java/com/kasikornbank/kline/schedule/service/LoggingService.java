package com.kasikornbank.kline.schedule.service;

import com.kasikornbank.kline.common.dto.LoggingDto;

public interface LoggingService {

    LoggingDto createLogging(LoggingDto loggingDto);

}
