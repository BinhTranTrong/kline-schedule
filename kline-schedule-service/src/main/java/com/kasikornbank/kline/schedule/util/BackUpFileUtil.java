package com.kasikornbank.kline.schedule.util;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.kasikornbank.kline.common.constant.LoggingConstant;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BackUpFileUtil {

    public BackUpFileUtil() {

    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Get directory list
     * @return directory list
     */
    public static List<String> getDirectoryList() {
        return Arrays.asList(LoggingConstant.ADMIN_PATH);
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Get list file name of specific directory
     * @param directory
     * @return Set<String> contain list file name
     */
    public static Set<String> getFileNames(String directory) {
        return Stream.of(Objects.requireNonNull(new File(directory).listFiles()))
                .filter(file -> (!file.isDirectory() && file.toString().endsWith(LoggingConstant.JSON_FILE)
                        && file.toString().matches(LoggingConstant.CONTAIN_NUMBER)))
                .map(File::getName)
                .collect(Collectors.toSet());
    }

    /**
     * @author binh_trong.t
     * @created 2562/04/18
     * @description Convert string to json object
     * @param data
     * @return JsonObject
     */
    public static JsonObject convertStringToJson(String data) {
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (data, JsonElement.class);
        return element.getAsJsonObject();
    }

}
