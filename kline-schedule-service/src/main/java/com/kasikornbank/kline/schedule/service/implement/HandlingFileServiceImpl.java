package com.kasikornbank.kline.schedule.service.implement;

import com.kasikornbank.kline.common.constant.LoggingConstant;
import com.kasikornbank.kline.schedule.service.HandlingFileService;
import com.kasikornbank.kline.schedule.util.CompressUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
public class HandlingFileServiceImpl implements HandlingFileService {

    @Autowired
    CompressUtil compressUtil;

    @Autowired
    Environment environment;

    @Override
    public void process() {
       String serverPort = environment.getProperty("server.port");
       String serverIP = environment.getProperty("server.address");
        String date =  LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd-HHmmSS"));
        log.info("=== Starting handling file service ===");
        try {
            compressUtil.zipDirectory(LoggingConstant.ADMIN_PATH,LoggingConstant.ZIP_POOL+"/"+LoggingConstant.ZIP_NAME + "_" + date + "_"+serverIP+"-"+serverPort+".zip");
            compressUtil.moveZipFiles(LoggingConstant.ZIP_POOL,LoggingConstant.ZIP_STORAGE_PATH);
        } catch (IOException e) {
            log.debug(e.getMessage());
            log.debug(e.getCause().getMessage());
        }finally {
            log.info("FINISH COMPRESSED.");
        }

    }
}
